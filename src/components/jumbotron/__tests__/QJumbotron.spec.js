import { shallowMount } from '@vue/test-utils'
import { QJumbotron } from '..'

const GRADIENT = 'rgba(34,193,195,1) 0%, rgba(253,187,45,1) 100%'

describe('QJumbotron', () => {
  test('render with radial gradient', () => {
    const wrapper = shallowMount(QJumbotron)
    wrapper.setProps({
      gradient: `circle, ${GRADIENT}`
    })
    expect(wrapper.vm.computedStyle).toBeDefined()
  })
  test('render with linear gradient', () => {
    const wrapper = shallowMount(QJumbotron)
    wrapper.setProps({
      gradient: GRADIENT
    })
    expect(wrapper.vm.computedStyle).toBeDefined()
  })
  test('render with image', () => {
    const wrapper = shallowMount(QJumbotron)
    wrapper.setProps({
      imgSrc: 'statics/mountains.jpg'
    })
    expect(wrapper.vm.computedStyle).toBeDefined()
  })
})
